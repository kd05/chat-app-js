// dom queries
const chatlist = document.querySelector(".chat-list");
const chatButtons = document.querySelector(".chat-rooms");
const nameForm = document.querySelector(".new-name");
const newChatForm = document.querySelector(".new-chat");
const updateMssg = document.querySelector(".update-mssg");


const username = localStorage.username ? localStorage.username : 'Anon'
const room = localStorage.room ? localStorage.room : 'general'

//class Instances
const chatroom = new Chatroom(room, username);
const chatUI = new ChatUI(chatlist);


//get chats and render
chatroom.getChats((data) => {
    console.log(data);
    chatUI.render(data);
});


// update chatroom
chatButtons.addEventListener('click', e => {
    if(e.target.tagName === 'BUTTON'){
        const room = e.target.getAttribute("id");
        chatroom.updateRoom(room);
        localStorage.room = room;
        chatUI.clearChat();
        chatroom.getChats((data) => {
            chatUI.render(data);
        });
    }
});




// Update Name
nameForm.addEventListener('submit', e => {
    e.preventDefault();
    const newName = nameForm.name.value.trim();
    chatroom.updateName(newName);
    nameForm.reset();
    updateMssg.innerHTML = `your name is updated to ${chatroom.username}`;
    setTimeout(() => {
        updateMssg.innerHTML = '';
    },3000)
});


// New Chat
newChatForm.addEventListener('submit', e => {
    e.preventDefault();
    const newMessage = newChatForm.message.value.trim();

    chatroom.addChat(newMessage)
        .then(() => newChatForm.reset())
        .catch((err) => console.log(err.message));
});
